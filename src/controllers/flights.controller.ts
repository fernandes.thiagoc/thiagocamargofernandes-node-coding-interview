import { JsonController, Get, Body, Post, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

interface AddPassengerToFlightBody {
    id: string;
}

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Post('/:flightId/passengers', { transformResponse:false })
    async addPassengerToFlight(@Body() body: AddPassengerToFlightBody, @Param('flightId') flightId: string) {
        await flightsService.addPassengerToFlight({ 
            flightId,
            passengerId: body.id
        });
        return {
            status: 201
        }
    }
}
