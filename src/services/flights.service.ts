import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'

interface AddPassengerToFlightInput { 
    flightId: string,
    passengerId: string,
}

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async addPassengerToFlight(input: AddPassengerToFlightInput): Promise<void> {
        await FlightsModel.updateOne(
            { _id: input.flightId },
            { $push: { passengers: input.passengerId } }
        )
    }
}
